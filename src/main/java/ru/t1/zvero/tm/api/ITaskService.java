package ru.t1.zvero.tm.api;

import ru.t1.zvero.tm.model.Project;
import ru.t1.zvero.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name, String description);

    Task create(String name);

    Task add(Task task);

    List<Task> findAll();

    void clear();

    void remove(Task task);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}