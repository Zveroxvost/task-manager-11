package ru.t1.zvero.tm.api;

import ru.t1.zvero.tm.model.Project;
import ru.t1.zvero.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    List<Task> findAll();

    void clear();

    Task create(String name, String description);

    Task create(String name);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}